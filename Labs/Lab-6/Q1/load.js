var jsonObj;
function addtoselect() {
    var data_file = "http://localhost:8000/data.json";
    var http_request = new XMLHttpRequest();


    http_request.onreadystatechange = function () {

        if (http_request.readyState == 4 && http_request.status == 200) {
            jsonObj = JSON.parse(http_request.responseText);
            var i;
            var list = [];
            for (i in jsonObj) {
                list.push(jsonObj[i].cuisine)
            }
            var cuisines = [...new Set(list)];
            console.log(list);
            console.log(cuisines);
            for (i in cuisines){
                document.getElementById("cuisine-list").innerHTML += '<option value="' + cuisines[i] + '">' + cuisines[i] + '</option>';
            }
        }
    }

    http_request.open("GET", data_file, true);
    http_request.send();
}
$(document).ready(function () {
    $("#cuisine-list").change(function (event) {
        $("#cuisine-body").html(" ");
        var cuisineName = $(this).val();
        console.log(cuisineName);
        console.log(jsonObj);
        for (i in jsonObj){
            if (jsonObj[i].cuisine == cuisineName){
                $("#cuisine-body").append('<tr> <td>' + jsonObj[i].name + '</td>'  + '</tr>');        
            }
        
        }
    });
});

addtoselect();