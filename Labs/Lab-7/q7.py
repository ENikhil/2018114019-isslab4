import os
import urllib.request
from bs4 import BeautifulSoup as BS
import requests
import sys
import os

num_imgs = sys.argv[1]
path = sys.argv[2]

url = "https://www.smbc-comics.com/comic/"

os.chdir(path)

for i in range(int(num_imgs)):
    src_code = requests.get(url)
    code = src_code.text
    soup = BS(code, 'html.parser')
    img_url = soup.find_all('img', {'id':'cc-comic'})[0].get('src')
    img_url = img_url.replace(" ", "%20")
    print("Downloading img from url " + str(img_url))
    urllib.request.urlretrieve(img_url, "comic" + str(i) + ".png")
    url = soup.find_all("a", class_="cc-prev")[0].get('href')
    print(url)