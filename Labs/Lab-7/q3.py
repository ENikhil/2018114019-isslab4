def UniqueSort(text):
    arr = text.split(', ')
    arrset = set(arr)
    arr = list(arrset)
    arr.sort()
    return arr

print(UniqueSort("apple, red, white, purple, red, orange, white"))