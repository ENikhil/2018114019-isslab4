def isLeapYear(year):
    return (True if (year % 100 == 0 and year % 400 == 0) or (year % 100 != 0 and year % 4 == 0)
            else False)

print(isLeapYear(3000))