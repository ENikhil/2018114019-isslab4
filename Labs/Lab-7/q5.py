from datetime import date

def GetDays(date1, date2):
    date1 = date(*map(lambda i: int(i), date1.split(',')))
    date2 = date(*map(lambda i: int(i), date2.split(',')))
    return (date2-date1).days

print(GetDays("2000,2,28", "2001,2,28"))