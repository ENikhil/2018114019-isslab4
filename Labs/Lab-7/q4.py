def JoinIntegers(arr):
    arr = map(lambda i: str(i), arr)
    text = ''.join(arr)
    return int(text)

print(JoinIntegers([1, 2, 3, 4, 1, 12, 4,1]))