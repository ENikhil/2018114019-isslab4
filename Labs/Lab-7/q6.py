import sys

input_file = sys.argv[1]
output_file = sys.argv[2]

file1 = open(input_file, 'r')
file2 = open(output_file, 'w')

text = file1.readlines()

for n, i in enumerate(text):
    lst = ".,;:!\"()?"
    text[n] = ''.join(list(map(lambda c: '' if c in lst else c, i)))
    text[n] = text[n].rstrip()

tokens = []

for i in text:
    arr = i.split(" ")
    for elem in arr:
        tokens.append(elem.lower())

dic = {}

for i in tokens:
    if i not in dic:
        dic[i] = 1
    else: dic[i] += 1

for key, elem in dic.items():
    file2.write(key + ":" + str(elem) + "\n")

file1.close()
file2.close()
